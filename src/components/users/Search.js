import React, { useState, useContext } from 'react';
import GithubContext from "../../context/github/githubContext";
import alertContext from "../../context/alert/alertContext";

const Search = (props) => {
    const githubContext = useContext(GithubContext);
    const AlertContext = useContext(alertContext);
    const [text, setText] = useState('');
    const { setAlert, clearUsers } = props;

    const onChange = (e) => {
        setText(e.target.value);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if(text === ''){
            AlertContext.setAlert('Please enter smthing', 'light');
        } else {
            githubContext.searchUsers(text);
            setText('');
        }
    };

    //Return function
    return (
        <div>
            <form onSubmit={onSubmit} className="form">
                <input type="text" name="text" placeholder="Search Users..." value={text} onChange={onChange}/>
                <input type="submit" value="Search" className="btn btn-dark btn-block" />
            </form>
            {githubContext.users &&
            <button className="btn btn-light btn-block" onClick={githubContext.clearUsers}>Clear</button>}
        </div>
    )
};

export default Search
