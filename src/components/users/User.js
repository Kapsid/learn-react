import React, { Fragment, useEffect, useContext } from 'react';
import Spinner from '../layout/Spinner';
import {Link} from 'react-router-dom';
import Repos from '../repos/Repos';
import githubContext from "../../context/github/githubContext";

const User = (props) => {

    const GithubContext = useContext(githubContext);
    const { getLoading, user, getUser, repos, getUserRepos } = GithubContext;

    useEffect(() => {
        getUser(props.match.params.login);
        GithubContext.getUserRepos(props.match.params.login);

    },[]);

    const {
        name,
        avatar_url,
        location,
        bio,
        blog,
        login,
        html_url,
        company,
        followers,
        following,
        public_repos,
        public_gists,
        hireable
    } = user;

    const { loading } = props;

    if(loading) return <Spinner />
    return (
        <div>
            <Fragment>
                <Link to='/' className='btn btn-light'>Back to search</Link>
                Hireable: {''}
                {hireable ? <i className="fas fa-check text-success" /> : <i className="fas fa-times-circle text-danger" />}
                <div className="card grid-2">
                    <div className="all-center">
                        <img src={avatar_url} alt='' className='round-img' style={{width: '150px'}} />
                        <h1>{name}</h1>
                        <p>Location: {location}</p>
                    </div>
                    <div>
                        {bio && <Fragment>
                            <h3>Bio</h3>
                            <p>{bio}</p>
                            </Fragment>
                            }
                            <a href={html_url} className='btn btn-dark'>Visit Github profile </a>
                            <ul>
                                <li>
                                    {login && <Fragment>
                                        <strong>Username:</strong> {login}
                                        </Fragment>}
                                </li>
                                <li>
                                    {company && <Fragment>
                                        <strong>Company:</strong> {company}
                                        </Fragment>}
                                </li>
                                <li>
                                    {blog && <Fragment>
                                        <strong>Wesbite:</strong> {blog}
                                        </Fragment>}
                                </li>
                            </ul>
                    </div>
                </div>
                <div className="card text-center">
                    <div className="badge badge-primary">Followers: {followers}</div>
                    <div className="badge badge-success">Following: {following}</div>
                    <div className="badge badge-light">Public repos: {public_repos}</div>
                    <div className="badge badge-dark">Public gists: {public_gists}</div>
                </div>
                <Repos repos={repos} />
            </Fragment>
        </div>
    );

}

export default User;
